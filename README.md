# Generate Mill Order List

A company uses excel files to track and record orders for their shop. There is no master list to view that they are taking from all of the receipts.

This script takes all of the orders placed in a folder, grabs all the relevant data, and puts it into a giant master list in excel.